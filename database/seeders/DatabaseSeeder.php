<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        \App\Models\User::factory(1)->create(['email' => 'uchamplin@example.net', 'password' => \Hash::make('password')]);

        foreach (range(1, 10) as $index) {
            \App\Models\Company::create([
                'name' => $faker->company,
                'email' => $faker->companyEmail,
                'created_at' => $faker->unixTime(),
                'website' => $faker->url,
            ]);
        }

        foreach (range(1, 180) as $index) {
            \App\Models\Employee::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->email,
                'created_at' => $faker->unixTime(),
                'phone' => $faker->phoneNumber,
                'companies_id' => rand(1, 10),
            ]);
        }
    }
}
