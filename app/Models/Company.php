<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 *
 * @property $id
 * @property $name
 * @property $email
 * @property $email_verified_at
 * @property $logo_path
 * @property $website
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 *
 * @property Employee[] $employees
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Company extends Model
{

  protected $table = 'companies';


  static $rules = [
    'name' => 'required',
    'email' => 'required',
  ];

  protected $perPage = 20;

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'email', 'logo_path', 'website'];

  public function getPhotoAttribute()
  {
    if (!empty($this->logo_path)) {
      return str_replace(env('ALT_STORE_PATH'), '', $this->logo_path);
    }

    return null; //or default image
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function employees()
  {
    return $this->hasMany('App\Models\Employee', 'companies_id', 'id');
  }
}
