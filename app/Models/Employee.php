<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Employee
 *
 * @property $id
 * @property $first_name
 * @property $last_name
 * @property $companies_id
 * @property $phone
 * @property $email
 * @property $email_verified_at
 * @property $created_at
 * @property $updated_at
 *
 * @property Company $company
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Employee extends Model
{
  protected $table = 'employees';

  static $rules = [
    'first_name' => 'required',
    'last_name' => 'required',
    'email' => 'required',
  ];

  protected $perPage = 10;

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = ['first_name', 'last_name', 'companies_id', 'phone', 'email'];


  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   */
  public function company()
  {
    return $this->hasOne('App\Models\Company', 'id', 'companies_id');
  }
}
