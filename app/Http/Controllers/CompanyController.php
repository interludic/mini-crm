<?php

namespace App\Http\Controllers;

use App\Models\Company;
use \App\Http\Requests\CompanyRequest;

/**
 * Class CompanyController
 * @package App\Http\Controllers
 */
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(10);

        return view('company.index', compact('companies'))
            ->with('i', (request()->input('page', 1) - 1) * $companies->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = new Company();
        return view('company.create', compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CompanyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        request()->validate(Company::$rules);
        $company = Company::create($request->all());

        if (!empty($input['file'])) {
            dd($input['file']);
            // Get the temporary path using the serverId returned by the upload function in `FilepondController.php`            
            $filepond = app(\Sopamo\LaravelFilepond\Filepond::class);
            $company->logo_path = $filepond->getPathFromServerId($input['file']);
            $company->save();
        }

        return redirect()->route('company.index')
            ->with('success', 'Company created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);

        return view('company.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);

        return view('company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CompanyRequest $request
     * @param  Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        request()->validate(Company::$rules);

        $company->update($request->all());

        $input = $request->input();

        if (!empty($input['file'][0])) {
            // Get the temporary path using the serverId returned by the upload function in `FilepondController.php`            
            $filepond = app(\Sopamo\LaravelFilepond\Filepond::class);
            $company->logo_path = $filepond->getPathFromServerId($input['file'][0]);
            $company->save();
        }

        return redirect()->route('company.index')
            ->with('success', 'Company updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $company = Company::find($id)->delete();

        return redirect()->route('company.index')
            ->with('success', 'Company deleted successfully');
    }
}
