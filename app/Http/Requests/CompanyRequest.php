<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CompanyRequest extends FormRequest
{
    public function forbiddenResponse()
    {
        return abort(403);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {
                    $rules = [
                        'name'      => 'required|max:255',
                        'email'          => 'required|unique:companies',
                        'logo_path'      => '',
                        'website'           => '',
                    ];
                }
            case 'PUT': {
                    $rules = [
                        'name'  => 'required|max:255',
                        'email'          => 'required|unique:companies',
                        'logo_path'      => '',
                        'website'           => '',
                    ];
                }
            case 'PATCH':
            default:
                break;
        }

        return $rules;
    }
}
