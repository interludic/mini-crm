<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Auth;

class EmployeeRequest extends FormRequest
{
    public function forbiddenResponse()
    {
        return abort(403);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) return true;

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {
                    return [
                        'first_name'      => 'required|max:255',
                        'last_name'      => 'required|max:255',
                        'companies_id'           => 'required',
                        'email'          => 'required|unique:employees',
                    ];
                }
            case 'PUT': {
                    return [
                        'first_name'  => 'required|max:255',
                        'last_name'      => 'required|max:255',
                        'companies_id'           => 'required',
                        'email'          => 'required|unique:employees',
                    ];
                }
            case 'PATCH':
            default:
                break;
        }
    }
}
