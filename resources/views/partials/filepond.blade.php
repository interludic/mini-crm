@section('head')        
    <link href="{{ asset('/css/filepond.min.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/filepond-plugin-image-preview.min.css')}}" rel="stylesheet">
@endsection        

<input type="file" class="upload-filepond-{{$formId??''}}" name="file[]" 
    data-allow-reorder="true"
    data-max-file-size="10MB"
    data-max-files="1" 
    accept="image/png, image/jpeg" 
    {{$required??''}}
/>
<input type="submit" class="submit{{$formId??''}}" style="display:none;">

@section('footer')      
    <!-- include jQuery library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <!-- include FilePond library -->
    <script src="{{ asset('/js/filepond/filepond.min.js')}}"></script>
    <!-- include FilePond plugins -->
    <script src="{{ asset('/js/filepond/filepond-plugin-image-preview.min.js')}}"></script>
    <script src="{{ asset('/js/filepond/filepond-plugin-image-transform.min.js')}}"></script>
    <script src="{{ asset('/js/filepond/filepond-plugin-file-validate-type.min.js')}}"></script>    
    <script src="https://unpkg.com/filepond-plugin-image-validate-size/dist/filepond-plugin-image-validate-size.js"></script>
    <!-- include FilePond jQuery adapter -->
    <script src="{{ asset('/js/filepond/filepond.jquery.js')}}"></script>
 
    <script>
        $(function(){            
            // First register any plugins
            $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
            $.fn.filepond.registerPlugin(FilePondPluginImageTransform);
            // $.fn.filepond.registerPlugin(FilePondPluginImageExifOrientation);
            // $.fn.filepond.registerPlugin(FilePondPluginFileValidateSize);
            $.fn.filepond.registerPlugin(FilePondPluginFileValidateType);
            $.fn.filepond.registerPlugin(FilePondPluginImageValidateSize);
            

            // Profile
            // FilePondPluginImageCrop,
            // FilePondPluginImageResize,     
                        
            $('.upload-filepond-{{$formId??''}}').filepond({
                labelIdle: 'Upload Files | <span class="filepond--label-action"> Browse </span>',
                allowMultiple: true,
                allowReorder: true,
                // https://pqina.nl/filepond/docs/api/plugins/image-transform/
                // imageTransformOutputQuality: 80,
                imageTransformOutputStripImageHead: false,
                allowFileTypeValidation: true,
                labelFileTypeNotAllowed: "Invalid File",
                acceptedFileTypes: ['image/*'],
                allowImageValidateSize: true,
                imageValidateSizeMinWidth: 100,
                imageValidateSizeMinHeight: 100,
            });
            
            $('.upload-filepond-{{$formId??''}}').filepond('setOptions', {
                server: {
                    url: '/api',
                    process: '/process',
                    revert: '/process',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                status: 4,
            });
            
            $('.upload-filepond-{{$formId??''}}').filepond('onprocessfile', function(e, file) {        
                // console.log(e);
                // console.log(file.serverId);
            });
            
            // Listen for addfile event
            $('.upload-filepond-{{$formId??''}}').on('FilePond:addfile', function(e) {
                // console.log('file added event', e);
            });
            
            $('#{{$formId??''}}Btn').on('click',function(e) {
                console.log('Fire! #{{$formId??''}} .submit{{$formId??''}}');
                e.preventDefault();
                
                if ($('.upload-filepond-{{$formId??''}}').filepond('status') == 3) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                    })

                    Toast.fire({
                    icon: 'warning',
                    title: 'Upload in progress, please wait before trying again!'
                    })
                }else{                    
                    // $('#{{$formId??''}}').submit(); //https://stackoverflow.com/questions/19280685/jquery-submit-function-doesnt-call-html5-validation
                    $('#{{$formId??''}} .submit{{$formId??''}}').click();
                }
            });
        
        });
    </script>

@endsection        