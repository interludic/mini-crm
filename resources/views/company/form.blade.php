<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('name') }}
            {{ Form::text('name', $company->name, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => 'Name']) }}
            {!! $errors->first('name', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('email') }}
            {{ Form::text('email', $company->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'Email']) }}
            {!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
             {{ Form::label('logo_path') }}
             {{--
            {{ Form::text('logo_path', $company->logo_path, ['class' => 'form-control' . ($errors->has('logo_path') ? ' is-invalid' : ''), 'placeholder' => 'Logo Path']) }}
            {!! $errors->first('logo_path', '<div class="invalid-feedback">:message</div>') !!}
                         --}}

                         {{$company->logo_path??''}} <br>
                         {{$company->photo}}
            @if (!empty($company->photo))
                <img src="{{env('APP_URL')}}/storage/{{$company->photo}}" alt="">
            @endif
            
            @include('partials.filepond', ['formId'=>'logo'])
        </div>
        <div class="form-group">
            {{ Form::label('website') }}
            {{ Form::text('website', $company->website, ['class' => 'form-control' . ($errors->has('website') ? ' is-invalid' : ''), 'placeholder' => 'Website']) }}
            {!! $errors->first('website', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>