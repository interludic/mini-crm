## Laravel Simple Project

As part of the job interview process we need to test basic Laravel skills. So the project should be simple, but at the same time touch majority of fundamentals. We believe 2-3 days is more than enough to complete this project. The project will be made available via our GIT repo, please clone it and push your own changes to your own GIT repo and then please share with us. All contact via developer@signaturegroup.com.au

With that in mind, here’s a project we have come up with.

Adminpanel to manage companies

Basically, project to manage companies and their employees. Mini-CRM.

x Basic Laravel Auth: ability to log in as administrator;

-   username: uchamplin@example.net
-   password: password

x CRUD functionality (Create / Read / Update / Delete) for tow menu items: Companies and Employees;
x Use Laravel’s pagination for showing Companies/Employees list, 10 entries per page;
x Use database seeds to create first user with email and password;
x Companies DB table consists of these fields: Name (required), email, logo, website;
x Employees DB table consists of these fields: First name (required), Last name (required), Company (foreign key to Companies), email, phone;
x Use database migrations to create those schemas above;

-   Store companies logos in store/app/public folder and make them accessible from public; logo (minimum 100x100)
    (^^^^'store' possibly a typo?)

x Use Laravel’s validation function, using Request classes;
x Use basic Laravel resource controllers with default methods, index, create, store etc…;
x Use Laravel make:auth as default Bootstrap-based design theme, but remove ability to register;
