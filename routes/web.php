<?php

use Illuminate\Support\Facades\Route;
use Sopamo\LaravelFilepond\Http\Controllers\FilepondController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resources([
    'employee' => App\Http\Controllers\EmployeeController::class,
    'company' => App\Http\Controllers\CompanyController::class,
]);

Route::prefix('api')->group(function () {
    Route::post('/process', [FilepondController::class, 'upload'])->name('pond.upload');
    Route::delete('/process', [FilepondController::class, 'delete'])->name('pond.delete');
});

Auth::routes(['register' => false]);
